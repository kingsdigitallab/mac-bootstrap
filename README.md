# Mac Bootstrap

This script provisions a (fresh) macOS install. It install any available software updates, installs software via [Homebrew](https://brew.sh/) and sets up some base preferences.

The script installs Python, Ruby and Node libraries that are commonly used in the Lab.

To configure which software and packages are installed edit the files in the `options` directory.

To run the script:

1. Clone or download this repository
1. Open the repository in Terminal
1. Run `./mac.sh`

While the script is running:

- The script may need to be run more than once if the macOS software updates require a restart.
- Keep an eye on the terminal output for potential issues installing software. If something fails to install it should be fine to re-run the script.

## Known issues

Some of the software installed with Homebrew may need kernel extensions enabled on the Mac. They can be enabled at:
**Systen Preferences -> Security & Privacy -> General**.
For more information about this see this [Apple Technical Note](https://developer.apple.com/library/content/technotes/tn2459/_index.html).
