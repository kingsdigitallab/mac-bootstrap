#!/usr/bin/env bash

function info {
    echo
    echo "--- ${1} ---" | fmt -c -w 80
    echo
}

# Ask for the administrator password upfront
sudo -v

# Keep-alive: update existing `sudo` time stamp until the script has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

info 'Updating macOS'
sudo softwareupdate -aiR

info 'xCode tools'
xcode-select --install

info 'Homebrew'
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

info 'Homebrew: taps'
brew tap caskroom/cask
brew tap caskroom/fonts

info 'Homebrew: brews'
xargs brew install < packages/brew.txt

info 'Homebrew: casks'
xargs brew cask install < packages/cask.txt

info 'Homebrew: fonts'
xargs brew cask install < packages/fonts.txt

info 'Homebrew: update'
brew update
brew upgrade
brew cask upgrade

info 'Homebrew: cleaning up'
brew cleanup

info 'Python'
pip3 install --upgrade pip
pip3 install -r packages/pip.txt

info 'Ruby'
xargs gem install < packages/gems.txt

info 'Node'
xargs npm -g install < packages/npm.txt

info 'Shell'
W_SHELL=$(
whiptail --title "Shell" --menu "Choose a shell configuration:" 12 80 4 \
    "bash" "Bash (Oh My Bash)"   \
    "fish" "Fish (Oh My Fish)"  \
    "zsh" "Zsh (Oh My Zsh)" \
    "nada" "Continue without making changes to the shell"  3>&2 2>&1 1>&3
)

case $W_SHELL in
    "bash")
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh)"
        echo '' >> ~/.bashrc
        echo 'export PATH=/usr/local/bin:$PATH' >> ~/.bashr
        info 'Visit https://github.com/ohmybash/oh-my-bash for Bash configuration options'
        ;;
    "fish")
        brew install fish
        echo '/usr/local/bin/fish' | sudo tee -a /etc/shells
        chsh -s $(which fish)
        curl -L https://get.oh-my.fish | fish
        omf install
        info 'Visit https://github.com/oh-my-fish/oh-my-fish for Fish configuration options'
        ;;
	"zsh")
        sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
        chsh -s $(which zsh)
        echo '' >> ~/.zshrc
        echo 'export PATH=/usr/local/bin:$PATH' >> ~/.zshrc
        info 'Visit https://github.com/robbyrussell/oh-my-zsh for ZSH configuration options'
        ;;
    *)
        info 'Leaving shell configuration as is'
        ;;
esac

info 'Editors'
W_EDITORS=$(
whiptail --title "Editors" --checklist "Select text editors:" 10 80 2 \
    "code" "Visual Studio Code" on \
    "subl" "Sublime Text (KDL has a license) " off 3>&2 2>&1 1>&3
)

W_EDITORS=$(echo "$W_EDITORS" | tr -d \")
brew cask install $W_EDITORS

info 'Dotfiles'
stow -t ~ dotfiles

info 'Preferences'
# Save screenshots in subfolder in the desktop
mkdir ${HOME}/Desktop/Screenshots
defaults write com.apple.screencapture location -string "${HOME}/Desktop/Screenshots"

# Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF)
defaults write com.apple.screencapture type -string "png"

# Enable subpixel font rendering on non-Apple LCDs
defaults write NSGlobalDomain AppleFontSmoothing -int 2

# Finder: show hidden files by default
defaults write com.apple.finder AppleShowAllFiles -bool true

# Finder: show all filename extensions
defaults write NSGlobalDomain AppleShowAllExtensions -bool true

# Show the ~/Library folder
chflags nohidden ~/Library

killall Finder
killall Dock

echo
echo 'Done. Note that some of these changes require a logout/restart of your OS to take effect.'
